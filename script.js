$(document).ready(function() {
  var account = {
    '1234' : {'pin': '1234', 'balance': 500},
    '5678' : {'pin': '5678', 'balance': 1500},
    '0000' : {'pin': '0000', 'balance': 2500}
  };

  $('.all').hide();
  $('#displaybalance').hide();

  $('.form-signin').submit(function() {
    var accountNo = $('#accountNo').val();
    var pin = $('#pin').val();
    if (accountNo != null && account[accountNo]['pin'] == pin) {
      $('.all').show();
      $('.form-signin').hide();
      $('#displaybalance').show();
      showbalance(accountNo);
    } else {
      alert('เลขที่บัญชีหรือรหัสประจำตัวไม่ถูกต้อง');
    }
    return false;
  })
  function showbalance(accountNo) {
    $("#balance").html(account[accountNo]['balance'].toString())
  }
  function deposit(accountNo, amount) {
    account[accountNo]['balance'] += amount
  }
})

